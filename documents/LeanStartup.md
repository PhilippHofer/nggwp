Main goal: how to build a sustainable business around a vision

> Formulate a vision

> Formulate a value hypothesis (test whether our product really delivers value)

> Formulate a growth hypothesis

> Define leap-of-faith questions

Answer these 4 questions:
1. Do consumers recognize that they have the problem you are trying to solve?
2. If there was a solution, would they buy it?
3. Would they buy it from us?
4. Can we build a solution for that problem?

Build -> Measure -> Learn (works the opposite way)

# Innovation Accounting
1. Use a MVP to establish real data on where the company is right now
2. Startups must attempt to tune the engine from the baseline toward the ideal. 
3. After  the  startup  has  made  all  the micro  changes  and  product  optimizations  it  can move  its  baseline  toward  the  ideal,  the  company  reaches  a  decision  point:  pivot  or preserve.

## 3 A's of metrics
1. Actionable (clear cause and effect)
2. Accessible (everyone must understand the reports)
3. Auditable (credible, i.e. one must be able to verify the credibility of the metric)

## Engine of growth
**Sticky** we want every user as often as possible and as long as possible on our site