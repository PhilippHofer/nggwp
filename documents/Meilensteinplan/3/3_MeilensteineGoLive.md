---
title: "Meilensteinplan GoLive"
author: [GGWP]
date: "Februar 2019"
titlepage: false
...

# Meilensteinplan Go Live

## Streams (10.02.2019)
Streams funktioniert für folgende Spiele:

- LoL
- Fifa
- Fortnite
- Rocket Leage
- Dota 2
- PubG
- Hearthstone
- Overwatch
- Starcraft 2
- Call of Duty
- World of Tanks
- CS GO

## Usermanagement (20.02.)
-  Anmelden
-  Registrieren
-  Passwort zurücksetzen
-  Google / FB anmelden (inkl. Nicknames)

## News (03.03)
- Mobil: Google (Bilder größer), Desktop: Redbull (variable Höhe und mit Tags)
-  2 Kategorien von Tags: Haupttags die in Übersicht abgezeigt wird + Nebentags nach denen zwar gefiltert werden kann aber nicht in Übersicht angezeigt werden.

## Personalisierung (Tags) - (10.03.)
- Stream -> zu meine Tags hinzufügen
- Benutzer kann eigene Tags hinzufügen und löschen
- Startseite -> eigene Filter vorausgewählt

## Turniere (20.03.)
Für folgende Spiele werden Turniere unterstützt:

- LoL (ähnlich zu https://watch.na.lolesports.com/standings/lec/lec_2019_spring/regular_season)
- Fifa
- Fortnite