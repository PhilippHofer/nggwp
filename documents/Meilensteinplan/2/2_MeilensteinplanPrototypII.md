---
title: "Meilensteinplan Prototyp 2"
author: [GGWP]
date: "Jänner 2019"
titlepage: false
...

# Meilensteinplan Prototyp 2 (P2)

P2 teilt sich auf 2 Bereiche auf: News und 2. Spiel (Fortnite).

## News
Für den P2 werden alle News händisch ins System eingepflegt (später evtl. teilweise automatisiert).

### Frontend
News sollen wie im folgenden Screenshot ersichtlich angezeigt werden:

![Anzeige der News](1.png)

Zusätzlich soll es über den Buttons `Recent` und `Top Stories` eine Liste aller verfügbaren Spiele angezeigt werden. Durch Klick darauf werden die News gefiltert. Es ist möglich nach mehreren Spielen zu filtern (multiselect).

Durch Klick auf einen Newseintrag soll folgende Seite kommen:

![Anzeige eines News-Eintrags](2.png)

Für P2 soll dabei fix für jeden Artikel 1 Bild angegeben werden müssen (-> nicht keines und nicht mehrere).

### Backend
Im P2 wird die Userverwaltung programmiert, es gibt drei verschiedene Typen:
- Normaler Benutzer
- Professionelle Benutzer
- Admins

Alle drei Kategorien können News-Artikel schreiben (inkl. 1 Bild). News von `normale Benutzer` werden nicht sofort veröffentlicht, ein `Admin` muss diese vorher akzeptieren. Der gesamte Workflow funktioniert im P2.

## 2. Spiel
LoL ist bereits im P1 integriert worden. Im P2 soll zusätzlich Fortnite-Streams angezeigt werden (inkl. alle Filterungen).

## Quick Overview + Deadlines
- Fortnite (inkl. alle Filterungen): 20.01.
- Userverwaltung mit 3 Kategorien (normal, professionell, admin): 23.01.
- Backend: News schreiben, inkl. Review von normalen Benutzern: 27.01.
- News-Übersichtsseite (siehe 1. Foto): 30.01.
- News-Eintragsseite (siehe 2. Foto): 04.02.
- News nach Spielen filtern: 04.02.