# Meeting 2019-02-05

## News-Seite
Übersicht wie https://www.redbull.com/at-de/tags/esports (variable Höhe + Tags)

Mobil: ähnlich zu Google, Bilder bissl größer

News: 2 Kategorien von Tags: Haupttags die in Übersicht abgezeigt wird + Nebentags nach denen zwar gefiltert werden kann aber nicht in Übersicht angezeigt werden.

Datum dazuaquetschn


## Minimale Funktionen GoLive
- Spiele
  - LoL
  - Fifa
  - Fortnite
  - Rocket Leage
  - Dota 2
  - PubG
  - Heartstone
  - Overwatch
  - Starcraft 2
- Usermanagement
  -  Anmelden
  -  Registrieren
  -  Passwort zurücksetzen
  -  Google / FB anmelden (inkl. Nicknames)
-  News schreiben
   -  "Experience Points"
   -  Kommentare für News (auch auf Kommentare antwortbar)
-  Streams (funktioniert bereits)
-  Personalisierung -> Tags einstellbar
   -  Stream -> zu meine Tags hinzufügen
   -  Startseite -> eigene Filter vorausgewählt
-  Aktuelle Turniere
   -  LoL: https://watch.na.lolesports.com/standings/lec/lec_2019_spring/regular_season
   -  Fifa
   -  Fortnite
-  Spielunterseite