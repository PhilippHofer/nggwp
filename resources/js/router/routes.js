const Welcome = () => import('~/pages/welcome').then(m => m.default || m)
const Login = () => import('~/pages/auth/login').then(m => m.default || m)
const Stream = () => import('~/pages/stream').then(m => m.default || m)
const Register = () => import('~/pages/auth/register').then(m => m.default || m)
const PasswordEmail = () => import('~/pages/auth/password/email').then(m => m.default || m)
const PasswordReset = () => import('~/pages/auth/password/reset').then(m => m.default || m)
const NotFound = () => import('~/pages/errors/404').then(m => m.default || m)

const Home = () => import('~/pages/home').then(m => m.default || m)
const Settings = () => import('~/pages/settings/index').then(m => m.default || m)
const SettingsProfile = () => import('~/pages/settings/profile').then(m => m.default || m)
const SettingsPassword = () => import('~/pages/settings/password').then(m => m.default || m)

const NewsPost = () => import('~/pages/news/post').then(m => m.default || m)
const NewsReview = () => import('~/pages/news/review').then(m => m.default || m)

const News = () => import('~/pages/news/index').then(m => m.default || m)
const NewsDetail = () => import('~/pages/news/show').then(m => m.default || m)



export default [
  { path: '/', name: 'welcome', component: Welcome },

  { path: '/login', name: 'login', component: Login },
  { path: '/register', name: 'register', component: Register },
  { path: '/stream/:id', name: 'stream', component: Stream },
  { path: '/password/reset', name: 'password.request', component: PasswordEmail },
  { path: '/password/reset/:token', name: 'password.reset', component: PasswordReset },

  { path: '/home', name: 'home', component: Home },
  { path: '/settings',
    component: Settings,
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: SettingsProfile },
      { path: 'password', name: 'settings.password', component: SettingsPassword }
    ] },

  { path: '/news/post', name: 'news.post', component: NewsPost },
  { path: '/news/review', name: 'news.review', component: NewsReview },


  { path: '/news', name: 'news', component: News },
  { path: '/news/:id', name: 'newsdetail', component: NewsDetail, props: true },

  { path: '*', component: NotFound }
]
