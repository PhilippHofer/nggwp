<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stream extends Model {
    protected $fillable = ['username', 'thumbnail', 'game_id', 'viewer', 'login_name'];

    public function game()
    {
        return $this->belongsTo('App\Game');
    }
}
