<?php

namespace App\Http\Controllers;

use App\Game;
use App\Http\Traits\PermissionTrait;
use App\News;
use App\Stream;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class NewsController extends Controller
{
    use PermissionTrait;

    public function index()
    {
        $news = News::where('published', '1')->with('tags')->get();
        return $news;
    }

    public function show($id){
        $news = News::where('published', '1')->where('id', '=', $id)->first();
        return $news;
    }

    public function postnews(Request $request)
    {
        $newsPost = new News($request->all());
        $newsPost->save();

        $this->handleTags($newsPost, $request);
        $this->publishPostIfAllowed($newsPost);
        $this->handleImage($newsPost, $request);

        $newsPost->save();
    }

    private function publishPostIfAllowed($newsPost){
        $user = Auth::user();
        if($this->userAllowedToPostNewsWithoutReview($user)){
            $newsPost->published = 1;
        }
    }


    private function handleImage(News $newsPost, Request $request){
        if($request->get('image'))
        {
            $image = $request->get('image');
            $filename = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            $name = '/imgs/news/uploads/'.$filename;
            \Image::make($request->get('image'))->save(public_path($name));
        }

        $newsPost->image_path = $name;
    }

    public function getPostsForReview()
    {
        $review = News::where('published', '0')->get();
        return $review;
    }

    public function accept($id){
        $news = News::find($id);
        $news->published = 1;
        $news->save();
    }

    public function reject($id){
        $news = News::find($id);
        $news->delete();
    }

    private function handleTags(News $newsPost, Request $request)
    {
        $this->handleTagsSingle($newsPost["maintags"], $request, 1);
        $this->handleTagsSingle($newsPost["secTags"], $request, 0);
    }

    private function handleTagsSingle($tags, News $newsPost, $main){
        foreach($tags as $tag){
            $t = Tag::find($tag);
            $newsPost->tags()->attach($t, ['main' => $main]);
        }
    }


}
