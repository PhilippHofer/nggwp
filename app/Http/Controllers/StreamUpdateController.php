<?php

namespace App\Http\Controllers;

use App\Game;
use App\Stream;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StreamUpdateController extends Controller
{
    public function index()
    {
        $games = Game::all();
        foreach ($games as $game) {
            $this->insertStreams($game);
        }
    }

    private function insertStreams($game)
    {
        $url = 'https://api.twitch.tv/helix/streams?game_id='.$game->twitch_game_id;
        $streams = $this->getTwitchData($url);

        $game->streams()->delete();

        $count = 0;
        foreach ($streams as $stream) {
            if ($count < 20){
                Log::info("sending request");
                $username = $stream["user_name"];
                $thumbnailUrl = $stream["thumbnail_url"];
                $thumbnailUrl = str_replace("{width}", 400, $thumbnailUrl);
                $thumbnailUrl = str_replace("{height}", 225, $thumbnailUrl);
                $viewer = $stream["viewer_count"];
                $login_name = $this->getLoginNameForUserId($stream["user_id"]);

                $stream = new Stream([
                    'username' => $username,
                    'thumbnail' => $thumbnailUrl,
                    'game_id' => $game->id,
                    'viewer' => $viewer,
                    'login_name' => $login_name
                ]);
                $stream->save();
            }
            $count++;
        }
    }

    private function getTwitchData($url)
    {
        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Client-ID: f7zsxexufshudt5bhutnmyycx4qt4n\r\n"
            ]
        ];

        $context = stream_context_create($opts);

        return json_decode(file_get_contents($url, false, $context), true)["data"];
    }

    private function getLoginNameForUserId($user_id)
    {
        sleep(0.2);
        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Client-ID: f7zsxexufshudt5bhutnmyycx4qt4n\r\n"
            ]
        ];

        $context = stream_context_create($opts);

        return json_decode(file_get_contents("https://api.twitch.tv/helix/users?id=".$user_id, false, $context), true)["data"][0]["login"];
    }
}
