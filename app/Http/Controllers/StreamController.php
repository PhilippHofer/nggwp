<?php

namespace App\Http\Controllers;

use App\Game;
use App\Stream;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StreamController extends Controller
{
    public function index()
    {
        $streams = Stream::orderBy('viewer','desc')->limit(20)->get();

        $users = array();
        foreach ($streams as $stream) {
            $user = array();
            $user["name"] = $stream->username;
            $user["thumb"] = $stream->thumbnail;
            $user["login_name"] = $stream->login_name;
            $user["gameimg"] = $stream->game()->first()->img;
            $user["gamename"] = $stream->game()->first()->name;
            $user["gameid"] = $stream->game()->first()->id;
            $userJson = json_encode(array($user));
            array_push($users, $userJson);
        }

        $ret = json_encode($users, true);
        return $ret;
    }

}
