<?php
namespace App\Http\Traits;

trait PermissionTrait {
    public function userAllowedToPostNewsWithoutReview($user) {
        if($user->hasRole('admin')) return true;
        if($user->hasRole('professional')) return true;

        return false;
    }
}
