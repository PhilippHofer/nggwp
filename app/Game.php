<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model {

    public function streams()
    {
        return $this->hasMany('App\Stream');
    }
}
