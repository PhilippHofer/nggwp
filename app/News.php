<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model {
    protected $fillable = ['headline', 'subline', 'content', 'published', 'image_path', 'url'];

    public function tags(){
        return $this->belongsToMany('App\Tag')->withPivot('main');
    }
}