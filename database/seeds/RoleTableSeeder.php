<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_professional = new Role();
        $role_professional->name = 'professional';
        $role_professional->description = 'News from this usergroup will not get reviewed.';
        $role_professional->save();

        $role_admin = new Role();
        $role_admin->name = 'admin';
        $role_admin->description = 'This usergroup has a lot of permissions.';
        $role_admin->save();
    }
}
