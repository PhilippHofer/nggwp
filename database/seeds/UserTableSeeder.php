<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_professional = Role::where('name', 'professional')->first();
        $role_admin  = Role::where('name', 'admin')->first();

        $user = new User();
        $user->name = 'Heinrich Müller';
        $user->email = 'user@ggwp.com';
        $user->password = bcrypt('secret');
        $user->save();

        $professional = new User();
        $professional->name = 'Max Mustermann (Professional User)';
        $professional->email = 'professional@ggwp.com';
        $professional->password = bcrypt('secret');
        $professional->save();
        $professional->roles()->attach($role_professional);

        $admin = new User();
        $admin->name = 'Sabine Musterfrau (Admin)';
        $admin->email = 'admin@ggwp.com';
        $admin->password = bcrypt('secret');
        $admin->save();
        $admin->roles()->attach($role_admin);
    }
}
