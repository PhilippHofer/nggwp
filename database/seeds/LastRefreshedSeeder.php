<?php

use Illuminate\Database\Seeder;

class LastRefreshedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('last_refreshed')->insert([
            'last_update' => '0',
        ]);
    }
}
