<?php

use Illuminate\Database\Seeder;

class GameTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('games')->insert([
            'name' => 'League of Legends',
            'img' => '/imgs/gameicon/lol.png',
            'twitch_game_id' => 21779,
        ]);
        DB::table('games')->insert([
            'name' => 'Fortnite',
            'img' => '/imgs/gameicon/fortnite.png',
            'twitch_game_id' => 33214,
        ]);
        DB::table('games')->insert([
            'name' => 'FIFA19',
            'img' => '/imgs/gameicon/fortnite.png',
            'twitch_game_id' => 506103,
        ]);
        DB::table('games')->insert([
            'name' => 'Rocket League',
            'img' => '/imgs/gameicon/fortnite.png',
            'twitch_game_id' => 30921,
        ]);
        DB::table('games')->insert([
            'name' => 'Dota2',
            'img' => '/imgs/gameicon/fortnite.png',
            'twitch_game_id' => 29595,
        ]);
        DB::table('games')->insert([
            'name' => 'PuBg',
            'img' => '/imgs/gameicon/fortnite.png',
            'twitch_game_id' => 493057,
        ]);
        DB::table('games')->insert([
            'name' => 'Hearthstone',
            'img' => '/imgs/gameicon/fortnite.png',
            'twitch_game_id' => 138585,
        ]);
        DB::table('games')->insert([
            'name' => 'Overwatch',
            'img' => '/imgs/gameicon/fortnite.png',
            'twitch_game_id' => 488552,
        ]);
        DB::table('games')->insert([
            'name' => 'Starcraft 2',
            'img' => '/imgs/gameicon/fortnite.png',
            'twitch_game_id' => 490422,
        ]);
        DB::table('games')->insert([
            'name' => 'Call Of Duty',
            'img' => '/imgs/gameicon/fortnite.png',
            'twitch_game_id' => 504462,
        ]);
        DB::table('games')->insert([
            'name' => 'World Of Tanks',
            'img' => '/imgs/gameicon/fortnite.png',
            'twitch_game_id' => 27546,
        ]);
        DB::table('games')->insert([
            'name' => 'CS:GO',
            'img' => '/imgs/gameicon/fortnite.png',
            'twitch_game_id' => 32399,
        ]);


    }
}
