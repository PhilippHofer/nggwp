<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->insert([
            'headline' => 'Headline Numero Uno',
            'subline' => 'Mehr Text zu diesen Artikel...',
            'content' => 'Even more text Even more text Even more text Even more text Even more text Even more text ',
            'image_path' => '/imgs/gameicon/lol.png',
            'published' => '1',
        ]);

        DB::table('news')->insert([
            'headline' => 'Headline #2',
            'subline' => 'Mehr Text zu diesen ArtikelArtikel...',
            'content' => 'Even more text EvenEven more text ',
            'image_path' => '/imgs/gameicon/lol.png',
            'published' => '1',
        ]);

        DB::table('news')->insert([
            'headline' => 'Headline 3',
            'subline' => 'Mehr Text zu diesendiesendiesen Artikel...',
            'content' => 'Even more text Even more text Even more text Even more text Even more text Even more texre text Even more text Even more text Even more texre text Even more text Even more text Even more texre text Even more text Even more text Even more texre text Even more text Even more text Even more text ',
            'image_path' => '/imgs/gameicon/lol.png',
            'published' => '1',
        ]);

        DB::table('news')->insert([
            'headline' => 'Headline Four',
            'subline' => 'Mehr Te Artikel...',
            'content' => 'Even more text Even more text Even more text Even more text Even more text Even more text ',
            'image_path' => '/imgs/gameicon/lol.png',
            'published' => '1',
        ]);

        DB::table('news')->insert([
            'headline' => 'Headline fhwl',
            'subline' => 'MText zu diesen Artikel...',
            'content' => 'Even more text Even more text Even more re text Even more re text Even more re text Even more re text Even more text Even more text Even more text Even more text ',
            'image_path' => '/imgs/gameicon/lol.png',
            'published' => '1',
        ]);
    }
}
