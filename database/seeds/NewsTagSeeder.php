<?php

use App\News;
use App\Tag;
use Illuminate\Database\Seeder;

class NewsTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $tag_lol  = Tag::where('name', 'League of Legends')->first();
        $first_news = News::find(1);

        $first_news->tags()->attach($tag_lol);

        $first_news->save();
    }
}
