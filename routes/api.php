<?php

use App\Stream;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/user', function (Request $request) {
        $user = $request->user();
        $user->roles = $user->roles()->pluck("name")->toArray();
        return $user;
    });

    Route::post('postnews', 'NewsController@postnews');

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
});


Route::get('getstreams', 'StreamController@index');
Route::get('updatestreams', 'StreamUpdateController@index');
Route::get('tags',  function(){
    return \App\Tag::all();
});

Route::get('redirect', 'Auth\LoginController@redirectToProvider');
Route::get('callback', 'Auth\LoginController@handleProviderCallback');

Route::get('news', 'NewsController@index');
Route::get('news/show/{id}', 'NewsController@show');

Route::group(['middleware' => 'auth:api, role:admin'], function () {
    Route::get('news/review', 'NewsController@getPostsForReview');
    Route::get('news/review/accept/{id}', 'NewsController@accept');
    Route::get('news/review/reject/{id}', 'NewsController@reject');
});

